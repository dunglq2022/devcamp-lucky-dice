import { Component } from "react";
import imageLucky from '../ImageLuky'

class LuckyDice extends Component{
    constructor(props){
        super(props)

        this.state = {
            changeDiceImg : imageLucky.luckyDice
        }
    }
    buttonChangeDice = () => {
        let randomDice = Math.floor(Math.random() * 6) + 1
        console.log(randomDice)
        this.setState({
            changeDiceImg : imageLucky[randomDice]
        })
    }

    render(){
        return(
            <div className="container">
                <div style={{width: 300}}>
                    <img src={this.state.changeDiceImg} width={300}/>
                    <br/>
                    <div className="text-center">
                        <button onClick={this.buttonChangeDice} className="btn btn-secondary mt-2 ">NÉM XÚC XẮC</button>
                    </div>                
                </div>
            </div>            
        )
    }
}

export default LuckyDice;