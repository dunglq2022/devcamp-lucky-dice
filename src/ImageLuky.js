import luckyDice from './asset/images/LuckyDiceImages/dice.png'
import luckyDice1 from './asset/images/LuckyDiceImages/1.png'
import luckyDice2 from './asset/images/LuckyDiceImages/2.png'
import luckyDice3 from './asset/images/LuckyDiceImages/3.png'
import luckyDice4 from './asset/images/LuckyDiceImages/4.png'
import luckyDice5 from './asset/images/LuckyDiceImages/5.png'
import luckyDice6 from './asset/images/LuckyDiceImages/6.png'

const imageLucky = {
    luckyDice, 1: luckyDice1, 2:luckyDice2, 3:luckyDice3, 4:luckyDice4, 5:luckyDice5, 6:luckyDice6
}

export default imageLucky;